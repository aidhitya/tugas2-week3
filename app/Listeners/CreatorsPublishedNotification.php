<?php

namespace App\Listeners;

use App\Events\BlogPublished;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Blog;
use App\Mail\BlogPublishedMail;
use Illuminate\Support\Facades\Mail;

class CreatorsPublishedNotification implements ShouldQueue
{
    public $blog;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Blog $blog)
    {
        $this->blog = $blog;
    }

    /**
     * Handle the event.
     *
     * @param  BlogPublished  $event
     * @return void
     */
    public function handle(BlogPublished $event)
    {
        Mail::to($event->blog->user)->send(new BlogPublishedMail($event->blog));
    }
}
