<?php

namespace App\Listeners;

use App\Blog;
use App\Mail\BlogCreatorCreatedMail;
use App\Events\BlogCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendCreatorNotification implements ShouldQueue
{
    public $blog;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Blog $blog)
    {
        $this->blog = $blog;
    }

    /**
     * Handle the event.
     *
     * @param  BlogCreated  $event
     * @return void
     */
    public function handle(BlogCreated $event)
    {
        Mail::to($event->blog->user)->send(new BlogCreatorCreatedMail($event->blog));
    }
}
