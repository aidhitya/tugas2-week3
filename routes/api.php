<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::namespace('Auth')->group(function () {
    Route::post('register', 'RegisterController');
    Route::post('login', 'LoginController');
    Route::post('logout', 'LogoutController');
});

Route::post('blog', 'BlogController@store')->middleware('auth:api');
Route::patch('blog/update/{blog}', 'BlogController@update')->middleware('auth:api');
